# Alphayax Helm charts

Helm charts I personally use for some services.

## How to use

### Via ArtifactHUB

Take a look on https://artifacthub.io/packages/search?user=alphayax

### Flux CD

Add the helm repository to your flux configuration:

```yaml
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: HelmRepository
metadata:
  name: alphayax
  namespace: your-namespace

spec:
  interval: 1h0m0s
  type: oci
  url: oci://registry-1.docker.io/alphayax/
```

Then, create the helm releases with the chart you want:

```yaml
---
apiVersion: helm.toolkit.fluxcd.io/v2beta2
kind: HelmRelease
metadata:
  name: NAME-OF-RELEASE
  namespace: YOUR-NAMESPACE

spec:
  interval: 10m0s
  chart:
    spec:
      chart: CHART-NAME
      sourceRef:
        kind: HelmRepository
        name: alphayax

  values: {}
```

## Open Source
- [Kubernetes](https://kubernetes.io/) charts are made for [Helm](https://helm.sh)
- Continuous Deployment with [Flux](https://fluxcd.io/)
- Documentation Generated with [HelmDocs](https://github.com/norwoodj/helm-docs)
