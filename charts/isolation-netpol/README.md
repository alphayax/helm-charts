# isolation-netpol

![Version: 0.3.2](https://img.shields.io/badge/Version-0.3.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.0.0](https://img.shields.io/badge/AppVersion-1.0.0-informational?style=flat-square)

A chart for isolating ingress traffic in namespaces

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| allowedNamespacesSelectorLabels | list | `[]` | List of label selectors for selecting allowed namespaces |
| namespaceLabels | object | `{}` | Map of labels to add to the current namespace |

