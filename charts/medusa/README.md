# medusa

> !! This is **NOT** the official medusa Helm Chart !!

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/medusa)](https://artifacthub.io/packages/search?repo=medusa)

![Version: 1.1.30](https://img.shields.io/badge/Version-1.1.30-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v1.0.22-ls213](https://img.shields.io/badge/AppVersion-v1.0.22--ls213-informational?style=flat-square)

TV show and music grabber

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| alphayax |  |  |

## Source Code

* <https://gitlab.com/alphayax/helm-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` |  |
| ingress.annotations | object | `{}` | Annotations to add to the ingress |
| ingress.className | string | `""` | Ingress Class Name to use |
| ingress.enabled | bool | `false` | Enable Ingress |
| ingress.host | string | `""` | DNS to use |
| ingress.tls.enabled | bool | `true` | Enable TLS |
| ingress.tls.secretName | string | `""` | secret name use for storing TLS keys If empty, a name will be generated |
| medusa.image.repository | string | `"linuxserver/medusa"` | Docker image to use for medusa. See https://hub.docker.com/r/linuxserver/medusa/tags/ |
| medusa.image.tag | string | `""` | Docker image tag to use for medusa. If not specified, chart appVersion will be used. |
| medusa.persistence.config.claimName | string | `""` | PVC name for medusa config |
| medusa.persistence.config.enabled | bool | `false` | Enable persistence for medusa config |
| medusa.persistence.downloads.claimName | string | `""` | PVC name for downloads |
| medusa.persistence.downloads.enabled | bool | `false` | Enable persistence for downloads |
| medusa.persistence.tvshows.claimName | string | `""` | PVC name for tvshows |
| medusa.persistence.tvshows.enabled | bool | `false` | Enable persistence for tvshows |
| medusa.pgid | string | `"1000"` | Group Identifier See https://github.com/linuxserver/docker-medusa?tab=readme-ov-file#user--group-identifiers |
| medusa.puid | string | `"1000"` | User Group Identifier See https://github.com/linuxserver/docker-medusa?tab=readme-ov-file#user--group-identifiers |
| medusa.resources | object | `{"requests":{"cpu":"10m","memory":"500Mi"}}` | Resource requests and limits |
| medusa.timezone | string | `"Etc/UTC"` | Timezone |
| nameOverride | string | `""` |  |

## Usage

### Helm CLI

To install medusa with Helm CLI, you can run the following command:

```bash
helm install my-medusa oci://registry-1.docker.io/alphayax/medusa
```

### Flux CD

[Flux](https://fluxcd.io/) will automatically update medusa for each new version pushed on the Helm repository.

To install medusa with Flux CD, you can create a HelmRepository resource:

```yaml
# alphayax.helmrepository.yaml
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: HelmRepository
metadata:
  name: alphayax
  namespace: medusa

spec:
  interval: 1h0m0s
  type: oci
  url: oci://registry-1.docker.io/alphayax/
```

Then, you can create a HelmRelease resource:

> Values have been setting up as EXAMPLE.

> The PVC have to be manually created before the HelmRelease installation.

```yaml
# medusa.helmrelease.yaml
---
apiVersion: helm.toolkit.fluxcd.io/v2beta2
kind: HelmRelease
metadata:
  name: medusa
  namespace: medusa

spec:
  interval: 30m0s
  chart:
    spec:
      chart: medusa
      reconcileStrategy: ChartVersion
      sourceRef:
        kind: HelmRepository
        name: alphayax

  values:
    ingress:
      enabled: true
      host: "medusa.alphayax.com"
      annotations:
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/tls-acme: "true"

    medusa:
      timezone: "Europe/Paris"
      persistence:
        config:
          enabled: true
          claimName: "medusa-config-pvc"
        tvshows:
          enabled: true
          claimName: "medusa-tvshow-pvc"
```
